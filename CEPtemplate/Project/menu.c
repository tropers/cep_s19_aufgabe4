#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "menu.h"
#include "spi.h"

uint8_t mainMenuInput(void)
{
    int i;
    scanf("%d", &i);
    if (i == 1)
    {
        SPI_CS1(1); // select chip1
        spiWriteByte(OPC_READ_DID);
        printf("\ndevice ID:\t%x\n\n", (spiReadByte() | (spiReadByte() << 8) | (spiReadByte() << 16) | spiReadByte() << 24));
        SPI_CS1(0);
        return MMAIN;
    }
    else if (i == 2)
    {
        return MSUB2;
    }
    else if (i == 3)
    {
        return MSUB3;
    }
    else if (i == 4)
    {
        return MSUB4;
    }
    else if (i == 5)
    {
        return MSUB5;
    }
    else if (i == 6)
    {
        return MSUB6;
    }
    else
    {
        printf("please select an option between 1 and 6\n");
        return mainMenuInput();
    }
}

uint8_t printMainMenu()
{
    printf("Please select your course of action:\n\n");
    printf("1:\tPrint device ID\n");
    printf("2:\tRead from device\n");
    printf("3:\tErase from device\n");
    printf("4:\tWrite to device ID\n");
    printf("5:\tWrite Bit PRBS sequence to device\n");
    printf("6:\tRead Bit PRBS sequence from device\n\n");
    return mainMenuInput();
}

int subMenuInputDec(uint32_t *ret)
{
    scanf("%d", ret);
    printf("\n");
    return *ret <= 0x800000;
}

int subMenuInputHex(uint32_t *ret)
{
    scanf("%x", ret);
    printf("\n");
    return *ret <= 0x800000;
}

uint8_t printSubMenu2(void)
{
    uint32_t startAddress, nBytes, chipID;

    printf("Ready to read from device.\n");
    printf("Please specify your parameters:\n");
    printf("chip ID: ");
    fflush(stdout);

    while (!subMenuInputDec(&chipID) || ((chipID != 1) && (chipID != 2)))
    {
        printf("unexpected input for chip ID: %d.\n", chipID);
        printf("please select a number between 1 and 2:\t");
        fflush(stdout);
    }

    printf("start address: ");
    fflush(stdout);

    while (!subMenuInputHex(&startAddress))
    {
        printf("invalid format for start adress (%x).\n", startAddress);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("number of bytes: ");
    fflush(stdout);

    while (!subMenuInputDec(&nBytes))
    {
        printf("invalid number of bytes to read (%d).\n\n", nBytes);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("reading %d bytes, starting at %x (chip %d):\t", nBytes, startAddress, chipID);

    printFromSpi(chipID, startAddress, nBytes);

    printf("\n\n");

    return MMAIN;
}

uint8_t printSubMenu3(void)
{
    uint32_t startAddress, nBytes, chipID;

    printf("Ready to erase from device.\n");
    printf("Please specify your parameters:\n");
    printf("chip ID: ");
    fflush(stdout);

    while (!subMenuInputDec(&chipID) || ((chipID != 1) && (chipID != 2)))
    {
        printf("unexpected input for chip ID: %d.\n", chipID);
        printf("please select a number between 1 and 2:\t");
        fflush(stdout);
    }

    printf("start address: ");
    fflush(stdout);

    while (!subMenuInputHex(&startAddress))
    {
        printf("invalid format for start adress (%x).\n", startAddress);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("number of bytes: ");
    fflush(stdout);

    while (!subMenuInputDec(&nBytes))
    {
        printf("invalid number of bytes to erase (%d).\n", nBytes);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("erasing %d bytes, starting at %x (chip %d):\t\n\n", nBytes, startAddress, chipID);

    spiEraseData(chipID, startAddress, nBytes);

    return MMAIN;
}

uint8_t printSubMenu4(void)
{
    uint32_t startAddress, nBytes, i, chipID;
    char data[DATASIZE];

    printf("Ready to write to the device.\n");
    printf("Please specify your parameters:\n");
    printf("chip ID: ");
    fflush(stdout);

    while (!subMenuInputDec(&chipID) || ((chipID != 1) && (chipID != 2)))
    {
        printf("unexpected input for chip ID: %d.\n", chipID);
        printf("please select a number between 1 and 2:\t");
        fflush(stdout);
    }

    printf("start address: ");
    fflush(stdout);

    while (!subMenuInputHex(&startAddress))
    {
        printf("invalid format for start adress (%x).\n", startAddress);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("data: ");
    fflush(stdout);
    scanf("%s", data);
    printf("\n");

    for (i = 0; i < DATASIZE; i++)
    {
        if (data[i] == '\0')
        {
            nBytes = i;
            break;
        }
    }

    printf("writing %d bytes, starting at %x (chip %d)\n\n", nBytes, startAddress, chipID);

    spiEraseData(chipID, startAddress, nBytes);
    spiWriteData(chipID, startAddress, data);

    return MMAIN;
}

uint8_t printSubMenu5(void)
{
    uint32_t startAddress, nBytes, chipID;

    printf("Ready to write to the device.\n");
    printf("Please specify your parameters:\n");
    printf("chip ID: ");
    fflush(stdout);

    while (!subMenuInputDec(&chipID) || ((chipID != 1) && (chipID != 2)))
    {
        printf("unexpected input for chip ID: %d.\n", chipID);
        printf("please select a number between 1 and 2:\t");
        fflush(stdout);
    }

    printf("start address: ");
    fflush(stdout);

    while (!subMenuInputHex(&startAddress))
    {
        printf("invalid format for start adress (%x).\n", startAddress);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("PRBS number of bytes: ");
    fflush(stdout);
    while (!subMenuInputDec(&nBytes) || nBytes + startAddress > 0x800000)
    {
        printf("invalid number of bytes (%d).\n", nBytes);
        printf("please try again:\t");
        fflush(stdout);
    }
    printf("\n");

    printf("writing a %u byte PRBS, starting at %x (chip %d)\n\n", nBytes, startAddress, chipID);

    spiEraseData(chipID, startAddress, nBytes);
    spiWriteDataPRBS(chipID, startAddress, nBytes);

    return MMAIN;
}

uint8_t printSubMenu6()
{
    uint32_t startAddress, nBytes, chipID;
    int32_t rc;

    printf("Ready to read and check PRBS from device.\n");
    printf("Please specify your parameters:\n");
    printf("chip ID: ");
    fflush(stdout);

    while (!subMenuInputDec(&chipID) || ((chipID != 1) && (chipID != 2)))
    {
        printf("unexpected input for chip ID: %d.\n", chipID);
        printf("please select a number between 1 and 2:\t");
        fflush(stdout);
    }

    printf("start address: ");
    fflush(stdout);

    while (!subMenuInputHex(&startAddress))
    {
        printf("invalid format for start adress (%x).\n", startAddress);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("number of bytes: ");
    fflush(stdout);

    while (!subMenuInputDec(&nBytes) || nBytes + startAddress > 0x800000)
    {
        printf("invalid number of bytes to read (%d).\n\n", nBytes);
        printf("please try again:\t");
        fflush(stdout);
    }

    printf("reading %d bytes, starting at %x (chip %d):\t\n\n", nBytes, startAddress, chipID);

    rc = spiReadDataPRBS(chipID, startAddress, nBytes);

    printf("\nlast return code of PRBS stream: %d\n\n", rc);

    return MMAIN;
}
