#include "spi.h"
#include "prbsutil.h"

void globalUnprotect(int chipID);
void writeEnable(int chipID);
void waitReady(int chipID);

int spiChipSelect(int id, int enable)
{
    if (id == 1)
    {
        SPI_CS1(enable); // Select / Deselect Macro for SPI 1
        return 0;
    }
    if (id == 2)
    {
        SPI_CS2(enable); // Select / Deselect Macro for SPI 2
        return 0;
    }
    return -1;
}

// Aus Codebeispiel von Prof. Dr. Schäfers
uint8_t spiReadByte(void)
{
    SPI->DR = SPI_DUMMY_B; // Write dummy byte for reading
    while (!(SPI->SR & SPI_SR_RXNE))
        ;           // Wait for SPI ready
    return SPI->DR; // Return read byte
}

uint16_t spiRead16bit(void)
{
    return spiReadByte() | spiReadByte() << 8; // Read two bytes
}

uint32_t spiRead32bit(void)
{
    return spiRead16bit() | spiRead16bit() << 16; // Read four bytes
}

uint8_t spiWriteByte(uint8_t data)
{
    SPI->DR = data; // Write byte to data register
    while (!(SPI->SR & SPI_SR_RXNE))
        ;           // Wait for SPI ready
    return SPI->DR; // Return read dummy byte
}

uint16_t spiWrite16bit(uint16_t data)
{
    return (spiWriteByte((uint8_t)(data >> 8)) << 8) | spiWriteByte((uint8_t)(data & 0xff)); // Write two bytes
}

uint32_t spiWrite32bit(uint32_t data)
{
    return (spiWrite16bit((uint16_t)(data >> 16)) << 16) | spiWrite16bit((uint16_t)(data & 0xffff)); // Write four bytes
}

void spiInit(void)
{
    // Set PortB.9 as output (Chip Select 1)
    GPIOB->MODER |= (GPIO_Mode_OUT << (2 * 9));
    // Set PortG.6 as output (Chip Select 2)
    GPIOG->MODER |= (GPIO_Mode_OUT << (2 * 6));

    // Set SPI Pins to alternate port function
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
    GPIOC->MODER |= (GPIO_Mode_AF << (2 * 10)) | (GPIO_Mode_AF << (2 * 11)) | (GPIO_Mode_AF << (2 * 12));
    GPIOC->OSPEEDR |= (GPIO_Fast_Speed << (2 * 10)) | (GPIO_Fast_Speed << (2 * 11)) | (GPIO_Fast_Speed << (2 * 12));
    // set alternate function mode for use with spi (RM0090 Chap 8.3.2)
    GPIOC->AFR[1] |= (GPIO_AF_SPI3 << (4 * 2)) | (GPIO_AF_SPI3 << (4 * 3)) | (GPIO_AF_SPI3 << (4 * 4));

    // Set SPI Configuration
    // enable clock for SPI (RM0090 Chap 6.3.13)
    RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
    // set SPI configuration(RM0090 Chap 28.5.1)
    SPI->CR1 = (SPI_CR1_SPE | SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CLK_DIV_2 | SPI_MODE_3);
    SPI_CS1(0); // Deselect SPI 1
    SPI_CS2(0); // Deselect SPI 2
}

int spiCheckDeviceID(uint32_t did)
{
    int correct;
    SPI_CS1(1);                        // Select SPI 1
    spiWriteByte(OPC_READ_DID);        // Send READ Opcode
    correct = (did == spiRead32bit()); // Read Device ID
    SPI_CS1(0);                        // Deselect SPI 1
    return correct;                    // Return Device ID
}

void printFromSpi(int chipId, uint32_t startAddress, uint32_t nBytes)
{
    int i;
    spiChipSelect(chipId, 1); // Select Chip

    spiWriteByte(OPC_READ_ARRAY);                         // Send READ ARRAY Opcode
    spiWriteByte((uint8_t)((startAddress >> 16) & 0xff)); // MSB
    spiWriteByte((uint8_t)((startAddress >> 8) & 0xff));  //write address
    spiWriteByte((uint8_t)(startAddress & 0xff));         // LSB
    spiWriteByte(SPI_DUMMY_B);                            // Send dummy bytes
    spiWriteByte(SPI_DUMMY_B);

//    for (i = 0; i < nBytes; i++)
//    {
//        printf("%c", spiReadByte());
//    }
    
    printf("\n"); // A new line for better readability
    
    for (i = 0; i < nBytes; i++)
    {
        if (i % 16 == 0) {
            printf("\n"); // New line every 16 bytes
        }
        
        printf("%02X ", spiReadByte());
    }
    
    spiChipSelect(chipId, 0); // Deselect Chip
}

uint8_t spiReadData(int chipID, uint32_t startAddress)
{
    uint8_t result;

    spiChipSelect(chipID, 1); // Select Chip

    spiWriteByte(OPC_READ_ARRAY);                         // Send READ ARRAY Opcode
    spiWriteByte((uint8_t)((startAddress >> 16) & 0xff)); // MSB
    spiWriteByte((uint8_t)((startAddress >> 8) & 0xff));  //write address
    spiWriteByte((uint8_t)(startAddress & 0xff));         // LSB
    spiWriteByte(SPI_DUMMY_B);                            // Send dummy bytes
    spiWriteByte(SPI_DUMMY_B);
    result = spiReadByte();
    spiChipSelect(chipID, 0); // Deselect Chip
    return result;
}

void spiEraseData(int chipID, uint32_t startAddress, uint32_t nBytes)
{
    uint32_t address;
    int32_t bytes_left; // Bytes left for deletion

    bytes_left = nBytes + (startAddress & (0xfff));
    address = startAddress & ~(0xfff); //align start address

    if ((!address) && (bytes_left <= 0x800000 && bytes_left >= 0x7FE001))
    { // If bytes left are entire chip => erase entire chip
        printf("chip erase...\n");
        writeEnable(chipID);      // Writeenable Chip
        globalUnprotect(chipID);  // Unprotect Chip
        writeEnable(chipID);      // Writeenable Chip
        spiChipSelect(chipID, 1); // Select Chip
        spiWriteByte(OPC_CERASE); // Write CHIP ERASE Opcode
        spiChipSelect(chipID, 0); // Deselect Chip
        waitReady(chipID);        // Wait for Chip ready
        return;
    }

    while (bytes_left > 0)
    {
        if (bytes_left >= SECTOR_SIZE //more than 64kb data to erase
            && (!(address & (0xf << 12))))
        { //address is at beginning of a sector

            writeEnable(chipID);     // Writeenable Chip
            globalUnprotect(chipID); // Unprotect Chip
            writeEnable(chipID);     // Writeenable Chip
                                     //64k erase
            printf("64k Block erase\n");
            spiChipSelect(chipID, 1);
            spiWriteByte(OPC_BERASE_64K);                    // Send ERASE 64K Opcode
            spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
            spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
            spiWriteByte((uint8_t)(address & 0xff));         // LSB
            spiChipSelect(chipID, 0);                        // Deselect Chip

            waitReady(chipID); // Wait for Chip

            address += 1 << 16; //set next address (64k alignment)

            bytes_left -= SECTOR_SIZE; //refresh bytes left counter
        }
        else if (bytes_left >= HALF_SECTOR_SIZE //more than 32kb data to erase
                 && ((!(address & (0xf << 12))) //address is at beginning of a sector
                     || ((address & (0x8 << 12)) >> 15)))
        { //(or) address is at beginning of half a sector

            writeEnable(chipID);
            globalUnprotect(chipID);
            //32k erase
            printf("32k Block erase\n");
            writeEnable(chipID);
            spiChipSelect(chipID, 1);
            spiWriteByte(OPC_BERASE_32K);                    // Send ERASE 32K Opcode
            spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
            spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
            spiWriteByte((uint8_t)(address & 0xff));         // LSB
            spiChipSelect(chipID, 0);

            waitReady(chipID); // Wait for Chip

            address += 1 << 15; //set next address (32k alignment)

            bytes_left -= HALF_SECTOR_SIZE; //refresh bytes left counter
        }
        else
        { //less than 64kb (or 32kb) of data to erase OR adress is not aligned

            writeEnable(chipID);
            globalUnprotect(chipID);

            //4k erase
            printf("4k Block erase\n");
            writeEnable(chipID);
            spiChipSelect(chipID, 1);
            spiWriteByte(OPC_BERASE_4K);                     // Send ERASE 4K Opcode
            spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
            spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
            spiWriteByte((uint8_t)(address & 0xff));         // LSB
            spiChipSelect(chipID, 0);

            waitReady(chipID); // Wait for Chip

            address += 0x1000; //-((1<<11)%address);                               //set next address  (4k alignment)

            bytes_left -= SIXTEENTH_SECTOR_SIZE; //refresh bytes left counter
        }
    }
}

void spiWriteData(int chipID, uint32_t startAddress, char *data)
{
    uint32_t i, address;

    address = startAddress;

    writeEnable(chipID);
    globalUnprotect(chipID);

    writeEnable(chipID);
    spiChipSelect(chipID, 1);
    spiWriteByte(OPC_BPROGRAM);                      // Send PROGRAM Opcode
    spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
    spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
    spiWriteByte((uint8_t)(address & 0xff));         // LSB

    i = 0;
    while (data[i] != '\0')
    {
        spiWriteByte((uint8_t)data[i++]); //send next byte to device until reaching \0
        address++;
        if (!(address % 0x1000))
        {                             // If 64k alignment reached
            spiChipSelect(chipID, 0); // Deselect

            waitReady(chipID); // Wait for Chip

            writeEnable(chipID);     // Writeenable Chip
            globalUnprotect(chipID); // Unprotect Chip

            writeEnable(chipID);                             // Writeenable Chip
            spiChipSelect(chipID, 1);                        // Select Chip
            spiWriteByte(OPC_BPROGRAM);                      // Send PROGRAM Opcode
            spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
            spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
            spiWriteByte((uint8_t)(address & 0xff));         // LSB
        }
    }
    spiChipSelect(chipID, 0); // Deselect Chip

    waitReady(chipID); // Wait for Chip
}

void spiWriteDataPRBS(int chipID, uint32_t startAddress, uint32_t length)
{
    uint32_t i, address;

    address = startAddress;

    writeEnable(chipID);     // Writeenable Chip
    globalUnprotect(chipID); // Unprotect Chip

    writeEnable(chipID);                             // Writeenable Chip
    spiChipSelect(chipID, 1);                        // Select Chip
    spiWriteByte(OPC_BPROGRAM);                      // Send PROGRAM Opcode
    spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
    spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
    spiWriteByte((uint8_t)(address & 0xff));         // LSB

    i = 0;

    //align the address to 0x100
    while (!(address % 0x100) && i < length)
    {
        spiWriteByte(getNextByteOutOfPRBSstream()); // Write next PRBS to flash
        i++;
        address++;
    }
    spiChipSelect(chipID, 0); // Deselect Chip

    waitReady(chipID); // Wait for Chip

    if (i >= length)
        return; //in case it already wrote the amount of data

    //start aligned to fill the rest with the prbs sequence
    writeEnable(chipID);     // Wait for Chip
    globalUnprotect(chipID); // Unprotect Chip

    writeEnable(chipID);                             // Writeenable Chip
    spiChipSelect(chipID, 1);                        // Select Chip
    spiWriteByte(OPC_BPROGRAM);                      // Send PROGRAM Opcode
    spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
    spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
    spiWriteByte((uint8_t)(address & 0xff));         // LSB

    while (i < length)
    {
        spiWriteByte(getNextByteOutOfPRBSstream()); //send next byte to device until reaching \0
        i++;
        address++;
        if (!(address % 0x100))
        {                             // 256 byte alignment reached
            spiChipSelect(chipID, 0); // Deselect Chip

            waitReady(chipID); // Wait for Chip

            writeEnable(chipID);     // Writeenable Chip
            globalUnprotect(chipID); // Unprotect Chip

            writeEnable(chipID);                             // Writeenable Chip
            spiChipSelect(chipID, 1);                        // Select Chip
            spiWriteByte(OPC_BPROGRAM);                      // Send PROGRAM Opcode
            spiWriteByte((uint8_t)((address >> 16) & 0xff)); // MSB
            spiWriteByte((uint8_t)((address >> 8) & 0xff));  //write address
            spiWriteByte((uint8_t)(address & 0xff));         // LSB
        }
    }
    spiChipSelect(chipID, 0); // Deselect Chip

    waitReady(chipID); // Wait for Chip
}

void globalUnprotect(int chipID)
{
    spiChipSelect(chipID, 1);    // Select Chip
    spiWriteByte(OPC_WRI_SREG1); //write status register 1
    spiWriteByte(0x00);          // Write 0x00
    spiChipSelect(chipID, 0);    // Deselect Chip
}

void writeEnable(int chipID)
{
    spiChipSelect(chipID, 1);   // Select Chip
    spiWriteByte(OPC_WRITE_EN); //set Write Enable Latch (WEL)
    spiChipSelect(chipID, 0);   // Deselect Chip
}

void waitReady(int chipID)
{
    uint8_t check;

    spiChipSelect(chipID, 1);    // Select Chip
    spiWriteByte(OPC_READ_SREG); // Send READ SREG Opcode

    check = 0;
    while (!check)
    {
        spiReadByte();
        check = ~(spiReadByte()) & 1; // Check for SPI ready
    }
    spiChipSelect(chipID, 0); // Deselect Chip
}

int32_t spiReadDataPRBS(int chipID, uint32_t startAddress, uint32_t length)
{
    uint8_t buf;
    int32_t rc;

    uint32_t i;
    for (i = 0; i < length; i++)
    {
        buf = spiReadData(chipID, startAddress + i);        // Read PRBS data
        rc = chkNextByteOutOfPRBSstream(buf); chk_rc(rc);   // Check if PRBS is correct
    }
    initPRBSstreamChecking(); // Init PRBS checking

    return rc;
}
